P : 	D C
		{
		finaliza();
		}
		;

D :		L _PTVIRG D
		| L _PTVIRG
		;

L :		T id {insertSymbTab($2.symbol, Variable); }
		| L _VIRGULA id { insertSymbTab($3.symbol, Variable);}
		;

T :		_TINT
		;

C :		_BEGIN B _END
		;

B : 	S _PTVIRG B
		| S
		;

S :		A | I | W | O ;

A :		id _ATRIB E 
		{
		$1.intval = searchSymbTab($1.symbol);
		gera(STO,$3.intval,$1.intval,NADA);
		printf("\n");}
		;

I :		_IF E M C
		{remenda($3.intval,JF,prox,NADA);}
		| _IF E M C M _ELSE C
		{ remenda($3.intval,JF,$2.intval,$5.intval+1,NADA);
		remenda($5.intval,J,prox,NADA,NADA);
		printf("\n");}
		;

W :		_DO N C _WHILE E
		{ remenda($5.intval,JF,$3.intval,prox,NADA);
		gera(J, $2.intval,NADA,NADA); 		
		printf("\n");}
		;

O :		_PRINT _LP E _RP
		{ gera(PRINT,$3.intval, NADA, NADA); 
		printf("\n");}
		;

E :		E _MAIS T
		{$$.intval = temp(); 
		gera(ADD, $1.intval, $3.intval,$$.intval);}
		| E _MENOS T
		{$$.intval = temp(); 
		gera(SUB, $1.intval, $3.intval,$$.intval);}
		| T {$$.intval = $1.intval;}
		;

T :		T _MULT F
		{$$.intval = temp();
		gera(MULT, $1.intval, $3.intval,$$.intval);}
		| T _DIVID F
		{$$.intval = temp();
		gera(DIV, $1.intval, $3.intval,$$.intval);}
		| F {$$.intval = $1.intval;}
		;

F :		LP E RP
		| id {$$.intval=insertSymbTab($1.symbol, Variable);}
		| n {$$.intval=insertSymbTab($1.symbol, Constant);}
		;